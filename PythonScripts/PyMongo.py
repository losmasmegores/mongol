from builtins import print

from pymongo import *
from pprint import pprint

# Per començar a treballar amb la nostra BBDD de MongoDB, hem de fer la connexió:
client = MongoClient("mongodb+srv://raulsanto23:St5EAi7WIk2YrMUs@morcicluster.lnwepm2.mongodb.net/")
# A continuació hem de seleccionar quina database farem servir
db = client.project
# Per últim, hem de seleccionar la collection amb la que volem treballar:
collection = db.pokemon
teams = db.team
# Per fer selects hem de fer servir el mètode find() o find_one()
pokemons = collection.find()
pokemon = collection.find_one()
# pprint(pokemon)

# bulbasaur = collection.find_one({'name':'Bulbasaur'}, {'_id':0, 'next_evolution':{'name':1}})
# pprint(bulbasaur['next_evolution'])

searches = ["Search Bulbasaur height",
            "Search 1 weaknesses",
            "Search Bulbasaur evol",
            "Search Ivysaur evol",
            "Search Venusaur evol",
            "Release 004"
            ]
for search in searches:
    print("-------------------------------"+search+"------------------------------")
    inputs = search.split(" ")
    isSearching = False
    isDeleating = False
    searchParam = ""
    searchValue = ""
    paramsToShow = {"_id":0}
    n0 = 0
    for i in inputs:
        if i == inputs[1]:
            try:
                int(i)
                for j in i:
                    n0 += 1
                n0 = 3 - n0
                searchParam = "num"
                paramsToShow.update({"name":1})
            except ValueError:
                searchParam = "name"
                paramsToShow.update({"name": 1})
            finally:
                searchValue = "0"*n0 + str(i);
        elif i.lower() == "search":
            isSearching = True
        elif i.lower() == "release":
            isDeleating = True
        elif i.lower() == "evol":
            paramsToShow.update({"next_evolution": 1})
            paramsToShow.update({"prev_evolution": 1})
        else:
            paramsToShow.update({i: 1})

    if isSearching:
        poke = collection.find_one({searchParam: searchValue},paramsToShow)
        pprint(poke)
    else:
        try:
            poke = teams.find_one({searchParam: searchValue},paramsToShow)
            contador = 0

            teams.delete_one({"name": poke["name"]})
            for i in teams.find():
                # pprint(i)
                contador += 1
            print(poke["name"]+" alliberat. Nombre de Pokémon: "+str(contador))
            # print(poke["name"])
            # print(contador)
        except:
            print("No s'ha trobat el pokemon")

    print("---------------------------------------------------------------------------")





