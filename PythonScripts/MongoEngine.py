import random
from enum import Enum

from mongoengine import *
from pprint import *
from datetime import datetime

client = connect(host="mongodb+srv://raulsanto23:St5EAi7WIk2YrMUs@morcicluster.lnwepm2.mongodb.net/project")


class Type(Enum):
    bug = "Bug"
    dark = "Dark"
    dragon = "Dragon"
    electric = "Electric"
    fairy = "Fairy"
    fight = "Fighting"
    fire = "Fire"
    flying = "Flying"
    ghost = "Ghost"
    grass = "Grass"
    ground = "Ground"
    ice = "Ice"
    normal = "Normal"
    poison = "Poison"
    Psychic = "Psychic"
    rock = "Rock"
    steel = "Steel"
    water = "Water"


class Pokemon(Document):
    num = StringField()
    name = StringField()
    img = StringField()
    type = ListField(EnumField(Type))
    height = FloatField()
    weight = FloatField()
    candy = StringField()
    candy_count = FloatField()
    egg = StringField()
    spawn_chance = FloatField()
    avg_spawns = FloatField()
    spawn_time = StringField()
    multipliers = ListField(FloatField())
    weaknesses = ListField(EnumField(Type))
    next_evolution = ListField(DictField())
    prev_evolution = ListField(DictField())


class Moves(Document):
    name = StringField()
    pwr = FloatField(min_value=0, max_value=180)
    type = ListField(EnumField(Type))
    meta = {'allow_inheritance': True}  # Line to allow inheritance


class Team(Document):
    num = StringField()
    name = StringField()
    type = ListField(EnumField(Type))
    catch_date = DateTimeField()
    cp = FloatField(required=True, decimal_places=2)
    hpMax = FloatField(min_value=200, max_value=1000, decimal_places=2)
    hp = FloatField()
    atk = IntField(min_value=10, max_value=50)
    defe = IntField(min_value=10, max_value=50)
    energy = IntField(min_value=0, max_value=100, default=0)
    moves = ListField(ReferenceField(Moves))
    candy = StringField()
    candy_count = FloatField()
    current_candy = FloatField(default=0)
    weaknesses = ListField(EnumField(Type))
    next_evolution = ListField(DictField())
    prev_evolution = ListField(DictField())


class FastMove(Moves):
    moveType = StringField(default="Fast")
    energyGain = IntField(min_value=0, max_value=20)


class ChargedMove(Moves):
    moveType = StringField(default="Charged")
    energyCost = IntField(min_value=33, max_value=100)


def random_moves():
    moves = Moves.objects()
    random_moves_list = []
    for i in range(0, 2):
        random_moves_list.append(moves[random.randint(0, len(moves) - 1)])
    return random_moves_list


def catch_pokemon(pokemon_name):
    pokemon = Pokemon.objects(name=pokemon_name).first()
    if pokemon is None:
        print("No existeix cap pokemon amb aquest nom")
    else:
        randhpmax = random.uniform(200, 1000)
        randatk = random.randint(10, 50)
        randdef = random.randint(10, 50)
        randcp = randhpmax + randatk + randdef
        prueba = random_moves()
        new_pokemon = Team(
            num=pokemon.num,
            name=pokemon.name,
            type=pokemon.type,
            catch_date=datetime.now(),
            cp=randcp,
            hpMax=randhpmax,
            hp=randhpmax,
            atk=randatk,
            defe=randdef,
            energy=0,
            moves=prueba,
            candy=pokemon.candy,
            candy_count=pokemon.candy_count,
            current_candy=0,
            weaknesses=pokemon.weaknesses,
            next_evolution=pokemon.next_evolution,
            prev_evolution=pokemon.prev_evolution
        )
        new_pokemon.save()
        print("Pokemon capturat correctament")


def give_candy(pokemon_name):
    pokemon = Team.objects(name=pokemon_name).first()
    if pokemon is None:
        print("No existeix cap pokemon amb aquest nom")
    else:
        if pokemon.current_candy < pokemon.candy_count-1:
            pokemon.current_candy += 1
            print(pokemon_name + " ha rebut un caramel, ara té " + str(pokemon.current_candy) + " caramels")
            pokemon.save()
        else:

            pokemon.cp += 100
            evolution_pokemon = Pokemon.objects(num=pokemon.next_evolution[0]["num"]).first()
            print(pokemon_name + " està evolucionant a" + evolution_pokemon.name + "!!")
            pokemon.num = evolution_pokemon.num
            pokemon.name = evolution_pokemon.name
            pokemon.current_candy = 0
            pokemon.candy_count = evolution_pokemon.candy_count
            pokemon.save()


# give_candy("Bulbasaur")

# water_move = Moves(
#     name="Water sus",
#     pwr=100,
#     type=[Type.water]
# )
# water_move.save()

catch_pokemon("Snorlax")
